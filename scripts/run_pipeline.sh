##Script con el que hay que utilizar bash ## David Martínez 21-11-20 ########

##Crear directorios ==> Al final lo he hecho uno por uno.

##Obtener secuencia:

echo "Descargando secuencia"
mkdir -p res/genome
wget -O res/genome/ecoli.fasta.gz ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz
gunzip -k res/genome/ecoli.fasta.gz
echo "Descarga finalizada"
echo

##Proceso a realizar una única vez:

echo
mkdir res/genome/star_index
echo "Corriendo la indexación mediante STAR..."
STAR --runThreadN 4 --runMode genomeGenerate --genomeDir res/genome/star_index/ --genomeFastaFiles res/genome/ecoli.fasta --genomeSAindexNbases 9
echo

##Proceso a realizar por cada muestra:

for sampleid in $(ls data/*.fastq.gz | cut -d "_" -f1 | sed 's:data/::' | sort | uniq)
do
    echo "Analizando muestra $sampleid"
    echo
    bash scripts/analyse_sample.sh $sampleid
    echo "Muestra $sampleid analizada"
    echo
done

##Ejecutar la prueba MultiQC a conjunto:

echo "Corriendo MultiQC"
mkdir -p out/multiqc
multiqc -o out/multiqc .
echo
echo "PROGRAMA DE ANÁLISIS FINALIZADO"
